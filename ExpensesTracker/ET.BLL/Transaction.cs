﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ET.BLL
{
    public class Entry
    {
        public int EntryId { get; private set; }
        public DateTime Date { get; private set; }
        public string Concept { get; private set; }
        public decimal TotalAmount { get; private set; }
        public List<Transaction> Movements { get; private set; }

        public Entry(int id, string concept, DateTime date, List<Transaction> movements)
        {
            TotalAmount = 0;
            EntryId = id;
            Concept = concept;
            Date = date;
            foreach (Transaction t in movements)
            {
                TotalAmount += t.Debit;
                TotalAmount -= t.Credit;
                t.Push();
            }
        }
    }

    public abstract class Transaction
    {
        public int TransactionId { get; private set; }
        public string Concept { get; private set; }
        public Account Acc { get; private set; }
        public decimal Credit { get; private set; }
        public decimal Debit { get; private set; }

        //Expense
        public Transaction(int id, string concept, decimal credit, Account acc)
        {
            TransactionId = id;
            Concept = concept;
            Credit = credit;
            Debit = 0;
            Acc = acc;
        }
        //Income
        public Transaction(int id, string concept, Account acc, decimal debit)
        {
            TransactionId = id;
            Concept = concept;
            Credit = 0;
            Debit = debit;
            Acc = acc;
        }

        public void Push()
        {
            decimal amount = (Debit > 0) ? Debit : -Credit;
            Acc.UpdateBalance(amount);
        }
    }
}
