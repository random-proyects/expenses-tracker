﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.BLL
{
    public class Account
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public decimal Balance { get; private set; }

        public void UpdateBalance(decimal amount)
        {
            Balance += amount;
        }
    }
}
