﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ET.BLL;

namespace ET.DataHandler
{
    public class EntrySaver
    {
        public string EntryLocation { get; private set; }
        public string TransactionsLocation { get; private set; }
        public Entry EntryToSave { get; private set; }

        public EntrySaver(string entrylocation, string transactionslocation, Entry entry)
        {
            EntryLocation = entrylocation;
            TransactionsLocation = transactionslocation;
            EntryToSave = entry;
        }

        public void SaveEntry()
        {
            Console.WriteLine($"Saved {EntryToSave.EntryId},{EntryToSave.Date},{EntryToSave.Concept} to {EntryLocation}");
            for(int i = 0; i < EntryToSave.Movements.Count; i++)
            {
                Console.WriteLine($"Saved {EntryToSave.Movements[i]} to {TransactionsLocation}");
            }
        }
    }
}
