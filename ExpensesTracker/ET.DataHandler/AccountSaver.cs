﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ET.BLL;

namespace ET.DataHandler
{
    public class AccountSaver
    {
        public string AccountLocation { get; private set; }
        public Account AccountToSave { get; private set; }
        public AccountSaver(string entrylocation, string transactionslocation, Account account)
        {
            AccountToSave = account;
        }

        public void SaveAccount()
        {
            Console.WriteLine($"Saved {AccountToSave.Id},{AccountToSave.Name},{AccountToSave.Balance} to {AccountLocation}");
        }
    }
}
